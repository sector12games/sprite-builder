Automated sprite effects using the Photoshop API.

Requires:
 - https://bitbucket.org/sector12games/sector12lib
 - Photoshop CC (2013) or higher