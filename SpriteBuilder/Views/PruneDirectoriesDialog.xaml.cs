﻿// -----------------------------------------------------------------------
// <copyright file="PruneDirectoriesDialog.xaml.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.Views
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows;

    /// <summary>
    /// Interaction logic for PruneDirectoriesDialog.xaml
    /// </summary>
    public partial class PruneDirectoriesDialog : Window, INotifyPropertyChanged
    {
        private List<DirectoryInfo> _directoriesToPrune;

        /// <summary>
        /// Initializes a new instance of the <see cref="PruneDirectoriesDialog"/> class.
        /// </summary>
        public PruneDirectoriesDialog()
        {
            InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// The PropertyChanged event for our current WPF application.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the total number of empty directories that will be deleted
        /// if the user clicks the "OK" button.
        /// </summary>
        public int DirectoriesToPruneCount
        {
            get
            {
                if (_directoriesToPrune != null)
                    return _directoriesToPrune.Count();
                else
                    return 0;
            }
        }

        /// <summary>
        /// Gets the list of directories to prune as a single, newline separated 
        /// string.
        /// </summary>
        public string DirectoriesToPruneText
        {
            get
            {
                if (_directoriesToPrune == null || !_directoriesToPrune.Any())
                    return "";

                StringBuilder sb = new StringBuilder();
                foreach (DirectoryInfo dir in _directoriesToPrune)
                {
                    sb.Append(dir.FullName);
                    sb.Append(Environment.NewLine);
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// <para>
        /// Shows this window as a modal dialog. The list of directories that will
        /// be pruned will be displayed in a text box. 
        /// </para>
        /// <para>
        /// Whether or not the user clicked the "OK" or "Cancel" button will be
        /// accessible via this.DialogResult (a bool) when the window is closed.
        /// http://stackoverflow.com/questions/505572/how-do-i-get-system-windows-showdialog-to-return-true/
        /// </para>
        /// </summary>
        /// <param name="directoriesToPrune"></param>
        /// <param name="owner"></param>
        public void ShowDialog(List<DirectoryInfo> directoriesToPrune, Window owner)
        {
            _directoriesToPrune = directoriesToPrune;
            OnPropertyChanged("DirectoriesToPruneCount");
            OnPropertyChanged("DirectoriesToPruneText");
            OnPropertyChanged("DirectoriesToIgnoreCount");
            OnPropertyChanged("DirectoriesToIgnoreText");

            Owner = owner;
            ShowDialog();
        }

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event.
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Fires when the "OK" button is clicked. This will close the dialog.
        /// Note that we don't need to have a similar event handler for "Cancel",
        /// because the cancel behavior is handled automatically via the 
        /// IsCancel="True" attribute.
        /// http://stackoverflow.com/questions/4536988/dialogresult-wpf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
