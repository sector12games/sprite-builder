﻿// -----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.Views
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Input;
    using Sector12Common.Helpers;
    using SpriteBuilder.DataModels;
    using SpriteBuilder.Logic;
    using SpriteBuilder.ViewModels;
    using Application = System.Windows.Application;
    using Button = System.Windows.Controls.Button;
    using MenuItem = System.Windows.Controls.MenuItem;
    using MessageBox = System.Windows.MessageBox;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel _viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Create our view model
            _viewModel = new MainWindowViewModel();
            DataContext = _viewModel;

            // The dispatcher is null until after InitializeComponent()
            _viewModel.UIDispatcher = Dispatcher;
        }

        /// <summary>
        /// Pay attention to when the application is terminating.
        /// This will happen if the user clicks the "exit" menu
        /// item, or if the user clicks the "x" in the top right.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // Code put in by default when visual studio auto-generated
            // this method.
            base.OnClosing(e);

            // Make sure our worker thread is shut down before closing the app.
            // If we don't do this, the worker thread could attempt updating the
            // gui, which will no longer exist. This will obviously cause errors.
            // We don't want to sleep here however - if the worker thread does
            // attempt an Invoke() on the UI thread, it will never get processed,
            // because we are keeping the UI locked up with a sleep. In other words,
            // our worker thread will hang because it's waiting for the UI, and 
            // the UI will hang because it's waiting for the worker. Instead,
            // just tell the worker thread that we want to abort, and cancel the
            // close operation for now. Once the worker thread has aborted and
            // is about to terminate, it will call Application.Current.Shutdown()
            // on the dispatcher thread, and this method will be called once more.
            _viewModel.IsClosing = true;
            _viewModel.IsAborting = true;

            // If the worker thread is not finished, don't close the window.
            // Don't sleep either. The worker thread will trigger a shutdown
            // operation once it has finished executing, and this method will
            // be called one more time.
            if (_viewModel.IsExecuting)
                e.Cancel = true;
        }

        /// <summary>
        /// Resets all sprite builder data. Also resets data within the
        /// ViewModel. It does not reset any data specified by the user 
        /// (checkboxes, save location, etc).
        /// </summary>
        private void Reset()
        {
            // Reset GUI data
            _viewModel.Reset();
        }

        /// <summary>
        /// Displays the about dialog.
        /// </summary>
        private void ShowAboutDialog()
        {
            AboutDialog d = new AboutDialog { Owner = this };
            d.ShowDialog();
        }

        /// <summary>
        /// Kick off the worker thread that will build all selected sprites.
        /// </summary>
        private void BuildSprites()
        {
            // First, scan each folder for images. Build a list of all images that
            // need to be created and/or overwritten.
            FolderPrep.DetermineBuildTasks(
                _viewModel.GetCheckedOriginalFolders(),
                _viewModel.GetCheckedEffectFolders(),
                _viewModel.IgnoreFolders);

            // Next, scan each directory to gather information on how many files
            // and directories are being ignored due to the "Ignored Directories"
            // filter on the UI.
            _viewModel.IgnoredFolderData = FolderPrep.GetIgnoredFolderData(
                _viewModel.GetCheckedOriginalFolders(),
                _viewModel.IgnoreFolders);

            // Next, display a build verification dialog. Show the user how many
            // items will be created and/or overwritten, and ask him to verify.
            // If the user chooses not to continue, don't start the build.
            if (!DisplayBuildVerificationDialog())
                return;

            // Build verification was successful. Prepare for build.
            PrepForBuild();

            // Start processing the request in a different thread
            ProgressOverlay control = (ProgressOverlay)ProgressOverlayAdorner.AdornerContent;

            Thread workerThread = new Thread(() => control.BuildSprites());
            workerThread.Start();
        }

        /// <summary>
        /// Displays a dialog showing which empty directories exist within the 
        /// checked effect folders. These empty directories will be removed if
        /// the user confirms the deletion on the dialog.
        /// </summary>
        private void PruneEmptyDirectories()
        {
            // Get the list of directories to prune
            List<DirectoryInfo> dirs = CleanOperations.GetDirectoriesToPrune(_viewModel);

            // Show a confirmation dialog
            PruneDirectoriesDialog d = new PruneDirectoriesDialog();
            d.ShowDialog(dirs, this);

            // Determine whether the "OK" or the "Cancel" button was pressed.
            // If the user pressed "OK", go ahead and delete the empty directories.
            if (d.DialogResult != null && d.DialogResult.Value && dirs.Count > 0)
            {
                // Delete the empty directories
                CleanOperations.PruneDirectories(dirs);

                // Check to see if any new empty directories were created by
                // the prune operation.
                List<DirectoryInfo> newEmptyDirs = CleanOperations.GetDirectoriesToPrune(_viewModel);

                if (newEmptyDirs.Any())
                {
                    // Show a success dialog, but ask the user if he wants to
                    // repeat the prune operation due to new empty directories
                    // being created.
                    MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                    string text = dirs.Count + " empty directories were successfully removed.\n\n" +
                        newEmptyDirs.Count + " additional empty directories were created as a result.\n" +
                        "Repeat prune operation?";
                    MessageBoxResult result = MessageBox.Show(
                        this,
                        text,
                        "Re-run Prune Operation?",
                        MessageBoxButton.YesNo);

                    // If the user clicked "Yes", start the whole prune process over.
                    // If not, we are finished.
                    if (result == MessageBoxResult.Yes)
                        PruneEmptyDirectories();
                }
                else
                {
                    // Show a success dialog
                    MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                    MessageBox.Show(
                        this,
                        dirs.Count + " empty directories were successfully removed.",
                        "Success",
                        MessageBoxButton.OK);
                }
            }
        }

        /// <summary>
        /// Displays a build verification dialog. Shows the user how many
        /// items will be created and/or overwritten, and asks him to verify.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool DisplayBuildVerificationDialog()
        {
            // Build the text that we will display in the verification dialog
            string text = _viewModel.ImagesToCreate + " new images will be created.";
            text += "\n";

            // Build overwrite text
            if (_viewModel.Overwrite)
                text += _viewModel.ImagesToOverwrite + " existing images will be re-created and overwritten.";
            else
                text += _viewModel.ImagesToOverwrite +
                    " images already exist, but will NOT be re-created and overwritten.";
            text += "\n\n";

            // Build ignored data text
            IgnoredFolderData data = _viewModel.IgnoredFolderData;
            text += data.IgnoredFolders.Count + " directories " +
                "(" +
                data.IgnoredSubdirectories.Count + " subdirectories, " +
                data.IgnoredImages.Count + " images" +
                ")" +
                " are being ignored.";
            text += "\n";

            // Build outdated image text
            if (_viewModel.DeleteOutdatedEffectImages)
                text += _viewModel.OutdatedImages + " outdated images will be cleaned up (deleted).";
            else
                text += _viewModel.OutdatedImages + " outdated images exist, but will NOT be deleted.";

            // Finish the text
            text += "\n\n";
            text += "Continue?";

            // Show the dialog
            MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
            MessageBoxResult result = MessageBox.Show(
                this,
                text,
                "Proceed With Build?",
                MessageBoxButton.OKCancel,
                MessageBoxImage.Exclamation);

            // Return the result
            if (result == MessageBoxResult.OK)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Performs some preparation steps that need to be done regardless of 
        /// whether we are running a full build or just rebuilding the .tmx files.
        /// </summary>
        private void PrepForBuild()
        {
            // First, reset everything
            Reset();

            // Save the directory locations for use next time the application
            // runs. In order to use this, you need to add a setting manually in
            // Properties/Settings.Settings.
            foreach (OriginalFolderDataModel o in _viewModel.GetCheckedOriginalFolders())
            {
                o.SaveSettingsProperty();
            }

            // Also, save the IgnoreDirectories string for the next time the
            // application runs. Chances are, it will be the same throughout
            // the lifetime of the project.
            Properties.Settings.Default.IgnoreFolders = _viewModel.IgnoreFolders;
            Properties.Settings.Default.Save();

            // Display the progress overlay.
            _viewModel.IsAdornerVisible = true;
            ProgressOverlay control = (ProgressOverlay)ProgressOverlayAdorner.AdornerContent;
            control.Initialize(_viewModel);
            control.Owner = this;
        }

        /// <summary>
        /// Checks various conditions to determine whether or not we can execute
        /// a build or a prune. An error status label is also updated when this
        /// is checked, to provide the user with more information as to why the
        /// buttons may not be clickable.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CanExecute()
        {
            // If the view model is not initialized yet, don't allow execution
            if (_viewModel == null)
                return false;

            // Don't allow the user to start a new crawl if one is already running.
            if (_viewModel.IsExecuting)
                return false;

            // Don't allow the user to start a new crawl if no original directories
            // have been selected. 
            if (!_viewModel.HasCheckedOriginalFolder())
            {
                _viewModel.ErrorStatusLabel = "No build directories have been selected!";
                return false;
            }

            // Don't allow execution unless at least one of the available effects 
            // has been selected.
            if (!_viewModel.HasCheckedEffectFolder())
            {
                _viewModel.ErrorStatusLabel = "No effects have been selected!";
                return false;
            }

            // Don't allow execution if any of the selected directories have errors.
            // For example, they may not contain an 'Originals' directory.
            if (_viewModel.GetCheckedOriginalFolders().Any(o => o.HasErrors))
            {
                _viewModel.ErrorStatusLabel = "Some of the selected directories have errors!";
                return false;
            }

            // Everything is in order. Allow execution and remove any error 
            // messages we might have been displaying.
            _viewModel.ErrorStatusLabel = "";
            return true;
        }

        /// <summary>
        /// Occurs when the "Exit" menu item is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Shut down the application.
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Whether or not the "Delete" WPF command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CanExecute();
        }

        /// <summary>
        /// Occurs when the one of our "Clean" buttons are clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PruneEmptyDirectories();
        }

        /// <summary>
        /// Whether or not the "Close" WPF command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Allow the user to exit the program at any time
            e.CanExecute = true;
        }

        /// <summary>
        /// Occurs when the play button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            BuildSprites();
        }

        /// <summary>
        /// Whether or not the "Play" WPF command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CanExecute();
        }

        /// <summary>
        /// Displays a directory picker dialog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChooseDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            // Get the data context of the sender. The datacontext of the button
            // is automatically inherited from it's parent, which in our case,
            // happens to the be the ItemsControl element.
            Button button = (Button)sender;
            OriginalFolderDataModel o = (OriginalFolderDataModel)button.DataContext;

            // Display a dialog for the user to pick a save location (a folder).
            // Make sure the dialog is centered when shown.
            System.Windows.Forms.FolderBrowserDialog d = new System.Windows.Forms.FolderBrowserDialog();

            // Try to default the open folder dialog to the location currently
            // specified in the text box. There is what appears
            // to be a bug on SOME versions of windows, and it's not consistent
            // across all computers. The open file dialog does not scroll to the
            // "SelectedPath" value. Read more about this bug here:
            // http://stackoverflow.com/questions/6942150/why-folderbrowserdialog-dialog-does-not-scroll-to-selected-folder
            if (o.IsRootDirectoryValid)
                d.SelectedPath = o.RootDirectory;
            MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
            d.ShowDialog();

            // Update our textbox to display the file location the user
            // just selected in the popup dialog.
            if (!string.IsNullOrWhiteSpace(d.SelectedPath))
                o.RootDirectory = d.SelectedPath;
        }

        /// <summary>
        /// MenuItem events bubble up, so I only have to subscribe my entire
        /// menu object to a single MenuItemClick event. All of the menu's 
        /// children will fire events that will be caught here as well.
        /// Just check the incoming event to see which menu item was the
        /// source of the event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // I implemented commands on all buttons and menu items
            // except the about menu item after I wrote this code.
            // So now, there is only one item being used here. However,
            // I decided to leave this code as is as an example for
            // future projects.
            MenuItem item = (MenuItem)e.OriginalSource;

            if (item.Equals(AboutMenuItem))
                ShowAboutDialog();
        }
    }
}