﻿// -----------------------------------------------------------------------
// <copyright file="ProgressOverlay.xaml.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.Views
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;
    using Sector12Common.Controls.ControlData;
    using Sector12Common.Helpers;
    using SpriteBuilder.DataModels;
    using SpriteBuilder.Logic;
    using SpriteBuilder.ViewModels;

    /// <summary>
    /// Interaction logic for ProgressOverlay.xaml
    /// </summary>
    public partial class ProgressOverlay : UserControl
    {
        private DispatcherTimer _timer;
        private DispatcherTimer _calculateStatisticsTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressOverlay"/> class.
        /// </summary>
        public ProgressOverlay()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets our primary ViewModel. Ties data to the front end 
        /// via binding.
        /// </summary>
        public MainWindowViewModel MainWindowViewModel { get; set; }

        /// <summary>
        /// Gets or sets the owner of this adorner.
        /// </summary>
        public Window Owner { get; set; }

        /// <summary>
        /// Initializes our progress overlay UI elements. This should be called from
        /// the primary UI thread, not the worker thread.
        /// </summary>
        /// <param name="vm"></param>
        public void Initialize(MainWindowViewModel vm)
        {
            // Setup our ProgressBars for use from a worker thread. This way, we
            // can update our progress bars without having to make sure every
            // call we make is executed on the dispatcher thread.
            MainWindowViewModel = vm;
            if (vm.DisableProgressBarReadabilityDelay)
                ProgressBars.InitializeForUseInWorkerThread(Dispatcher);
            else
                ProgressBars.InitializeForUseInWorkerThread(Dispatcher, Globals.ReadingDelay);

            // Set up our time elapsed timer.
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(1000) };
            _timer.Tick += Timer_Tick;

            // Set up our statistics timer.
            _calculateStatisticsTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(Globals.CalculateStatisticsFrequency)
            };
            _calculateStatisticsTimer.Tick += CalculateStatisticsTimer_Tick;
        }

        /// <summary>
        /// Starts executing the build. This method should be called from within
        /// a separate thread.
        /// </summary>
        public void BuildSprites()
        {
            // Reset everything and start timers, etc.
            UI(() => BuildStarted());

            // Populate our progress bars with tasks
            UI(() => InitializeProgressBars());

            // Open photoshop if it's not already running
            ExecuteTask(() => PhotoshopCalls.InitializePhotoshop(MainWindowViewModel.HidePhotoshopWhileBuilding));
            MainWindowViewModel.HasPhotoshopBeenInitialized = true;

            // Prepare original directories for the upcoming build
            ExecuteTask(() => PrepareEffectDirectoriesForBuild());

            // Build each effect for each original sprite.
            IEnumerable<OriginalFolderDataModel> checkedOriginals = MainWindowViewModel.GetCheckedOriginalFolders();
            foreach (OriginalFolderDataModel original in checkedOriginals)
            {
                // Execute all of the build tasks that we previously determined
                // for the original folder.
                foreach (BuildTasks tasks in original.BuildTasksList)
                {
                    // Run the effect on all images that need to be freshly created
                    BuildEffectForAllSprites(original.RootDirectory, tasks.ImagesToCreate, tasks.EffectFolder);

                    // Run the effect on all images that previously existed and need
                    // to be overwritten. In fact, they have already been deleted at
                    // this point, and just need to be re-created.
                    if (MainWindowViewModel.Overwrite)
                        BuildEffectForAllSprites(original.RootDirectory, tasks.ImagesToOverwrite, tasks.EffectFolder);
                }
            }

            // Stop all of our timers, update the build result
            BuildCompleted();
        }

        /// <summary>
        /// Populates our progress bars with tasks.
        /// </summary>
        private void InitializeProgressBars()
        {
            // Add some basic primary tasks
            ProgressBars.Tasks.Add(new ProgressBarTask("Initializing Photoshop...", "", 0));
            ProgressBars.Tasks.Add(new ProgressBarTask("Preparing Effect Directories...", "", 0));

            // Populate our progress bars with build tasks and a subtask count. 
            // We will set the subtask text later, on the fly.
            IEnumerable<OriginalFolderDataModel> checkedOriginals = MainWindowViewModel.GetCheckedOriginalFolders();
            foreach (OriginalFolderDataModel original in checkedOriginals)
            {
                foreach (BuildTasks tasks in original.BuildTasksList)
                {
                    // Calculate the total number of images we will have to create. 
                    // Always include the images to create. Only include the images
                    // to overwrite if the user has specified "overwrite".
                    int totalSubtasks = tasks.ImagesToCreate.Count;
                    if (MainWindowViewModel.Overwrite)
                        totalSubtasks += tasks.ImagesToOverwrite.Count;

                    // Add a primary task to the progress bars. However, only add the 
                    // task if there are images to build.
                    if (tasks.ImagesToCreate.Count > 0 || (MainWindowViewModel.Overwrite && tasks.ImagesToOverwrite.Count > 0))
                    {
                        ProgressBarTask task = new ProgressBarTask(
                            "Building " + original.Name,
                            "Building " + tasks.EffectFolder.Name + " Effect...",
                            totalSubtasks);
                        ProgressBars.Tasks.Add(task);
                    }
                }
            }

            // Show our progress bars
            UI(() => ProgressBars.Visibility = Visibility.Visible);
        }

        /// <summary>
        /// Executes a single task and calls the NextTask() method on the progress
        /// bars control. This method will also check for any pauses or aborts before
        /// executing the given task.
        /// </summary>
        /// <param name="methodToExecute"></param>
        /// <param name="subtaskName"></param>
        /// <param name="subtaskDetails"></param>
        private void ExecuteTask(Action methodToExecute, string subtaskName = "", string subtaskDetails = "")
        {
            // Check for a pause or an abort. Wait if the application is paused,
            // and return if the user has aborted the build.
            CheckForPause();
            if (MainWindowViewModel.IsAborting)
                return;

            // Update the progress bars
            ProgressBars.NextTask(subtaskName, subtaskDetails);

            // Execute the method
            methodToExecute();
        }

        /// <summary>
        /// Builds the effect for each sprite specified in the originalImagePathList.
        /// </summary>
        /// <param name="rootDirectory"></param>
        /// <param name="originalImagePathList"></param>
        /// <param name="effect"></param>
        private void BuildEffectForAllSprites(
            string rootDirectory,
            IEnumerable<string> originalImagePathList,
            EffectFolderDataModel effect)
        {
            foreach (string path in originalImagePathList)
            {
                // We need to open the original file in photoshop, and then save it
                // to a different location. In order to do this we need to know 
                // the absolute paths to the original file and the new save file.
                string absolutePathOrigin = rootDirectory + "\\" + Globals.OriginalsDirectory + "\\" + path;
                string absolutePathDestination = rootDirectory + "\\" + effect.FolderName + "\\" + path;

                // Build the effect
                ExecuteTask(
                    () => BuildEffectForSprite(absolutePathOrigin, absolutePathDestination, effect.PhotoshopActionName),
                    "Building " + path);
            }
        }

        /// <summary>
        /// Builds a single sprite.
        /// </summary>
        /// <param name="absolutePathOrigin"></param>
        /// <param name="absolutePathDestination"></param>
        /// <param name="actionName"></param>
        private void BuildEffectForSprite(string absolutePathOrigin, string absolutePathDestination, string actionName)
        {
            // Make sure the directory path exists before we attempt to save
            // a file to it.
            string destinationDirectoryName = Path.GetDirectoryName(absolutePathDestination);
            if (destinationDirectoryName != null)
            {
                DirectoryInfo destinationDirectory = new DirectoryInfo(destinationDirectoryName);
                if (!destinationDirectory.Exists)
                    destinationDirectory.Create();
            }

            // Open the original file in photoshop
            PhotoshopCalls.LoadPhotoshopFile(new FileInfo(absolutePathOrigin));

            // Run the macro and save the file to disk
            PhotoshopCalls.ExecuteAndSavePhotoshopAction(absolutePathDestination, actionName);

            // Tell photoshop to close the original file
            PhotoshopCalls.CloseCurrentDocumentWithoutSaving();

            // Increment the total number of images that have been created
            MainWindowViewModel.ImagesCreated++;

            // If the user has specified that he'd like to use a short delay to 
            // keep photoshop responsive, sleep our background thread for just
            // a bit.
            if (MainWindowViewModel.UseDelayForPhotoshopResponsiveness)
                Thread.Sleep(Globals.PhotoshopResponsivenessDelay);
        }

        /// <summary>
        /// Prepares all "original" folders for the build. All images created
        /// by the SpriteBuilder will be stored in separate directories, with
        /// the same name as the original image. For example:
        ///   - Originals/Actor4/$Actor4.png     (original)
        ///   - Transparency/Actor4/$Actor4.png  (created by SpriteBuilder)
        ///   - BrushStroke/Actor4/$Actor4.png   (created by SpriteBuilder)
        /// </summary>
        private void PrepareEffectDirectoriesForBuild()
        {
            // Iterate over each "original" folder
            IEnumerable<OriginalFolderDataModel> checkedOriginals = MainWindowViewModel.GetCheckedOriginalFolders();
            foreach (OriginalFolderDataModel original in checkedOriginals)
            {
                // If any old files exist from a previous build, we need to delete them.
                // I've chosen to delete all files that need to be overwritten before
                // the build begins (instead of leaving them, and letting photoshop
                // automatically overwrite them) because I don't want the possibility of 
                // the program crashing, and then not knowing which files have been 
                // overwritten and which have not. If I delete all images first and the 
                // program crashes, I can resume where I left off by not selecting overwrite,
                // and only creating new images that do not already exist.
                foreach (BuildTasks tasks in original.BuildTasksList)
                {
                    // Get the effect directory we are dealing with. Remember that
                    // the effect directory is relative to the original directory.
                    string path = original.RootDirectory + "\\" + tasks.EffectFolder.FolderName;
                    DirectoryInfo dir = new DirectoryInfo(path);

                    // If the user has specified overwrite, delete any images that 
                    // will need to be re-created (overwritten) before the build.
                    if (MainWindowViewModel.Overwrite && dir.Exists)
                        DeleteFilesToOverwrite(tasks);

                    // If the user has opted to delete outdated (orphaned) effect
                    // files, delete them as well.
                    if (MainWindowViewModel.DeleteOutdatedEffectImages && dir.Exists)
                        DeleteOutdatedFiles(tasks);

                    // Create the effect directory if it does not already exist. 
                    // If the directory already exists, the following command will 
                    // do nothing.
                    dir.Create();
                }
            }
        }

        /// <summary>
        /// Deletes all files that we will need to overwrite before the build starts.
        /// I've chosen to delete all files that need to be overwritten before
        /// the build begins (instead of leaving them, and letting photoshop
        /// automatically overwrite them) because I don't want the possibility of 
        /// the program crashing, and then not knowing which files have been 
        /// overwritten and which have not. If I delete all images first and the 
        /// program crashes, I can resume where I left off by not selecting overwrite,
        /// and only creating new images that do not already exist.
        /// </summary>
        /// <param name="tasks"></param>
        private void DeleteFilesToOverwrite(BuildTasks tasks)
        {
            foreach (string path in tasks.ImagesToOverwrite)
            {
                // Determine the absolute path to the image. This is a combination
                // of the original root folder, the effect folder name, and the 
                // relative path that was generated earlier.
                string absolutePath = tasks.OriginalFolder.RootDirectory + "\\" +
                    tasks.EffectFolder.FolderName + "\\" + path;

                // Delete the image
                FileInfo file = new FileInfo(absolutePath);
                FileSystemHelper.DeleteFileWaitForAccess(file);
            }
        }

        /// <summary>
        /// Delete any outdated (orphaned) files that might exist in the effect
        /// folders. Orphaned files are effect images that no longer have an
        /// original.
        /// </summary>
        /// <param name="tasks"></param>
        private void DeleteOutdatedFiles(BuildTasks tasks)
        {
            foreach (string path in tasks.OutdatedImages)
            {
                // Determine the absolute path to the image. This is a combination
                // of the original root folder, the effect folder name, and the 
                // relative path that was generated earlier.
                string absolutePath = tasks.OriginalFolder.RootDirectory + "\\" +
                    tasks.EffectFolder.FolderName + "\\" + path;

                // Delete the image
                FileInfo file = new FileInfo(absolutePath);
                FileSystemHelper.DeleteFileWaitForAccess(file);
            }
        }

        /// <summary>
        /// <para>
        /// Calculates some statistics every few seconds. At the moment, we're only 
        /// calculating memory usage (from both the WPF app and photoshop). 
        /// Disk space usage is now only calcualted at the beginning and end of 
        /// the build.
        /// </para>
        /// <para>
        /// As a side note, there are a ton of ways to calculate and report current
        /// memory usage for a process, and none of them are really 100% accurate.
        /// Ultimately, proc.WorkingSet gives me roughly the equivilent to what
        /// is displayed in the Windows Task Manager (which is also not really 100%
        /// correct). Also note that there are multiple columns you can select to
        /// show in the task manager, which all give slightly different memory usage
        /// values (View -> Select Columns). The default for my computer was 
        /// "Memory - Private Working Set". Anyway, for more info regarding memory
        /// usage statistics, read the following article:
        /// http://stackoverflow.com/questions/1984186/what-is-private-bytes-virtual-bytes-working-set
        /// </para>
        /// </summary>
        private void CalculateStatistics()
        {
            // Calculate the memory usage for the SpritBuilder process
            Process proc = Process.GetCurrentProcess();
            long wpfMemory = proc.WorkingSet64;

            // Calculate the memory usage for photoshop.
            Process photoshop = Process.GetProcessesByName("photoshop").FirstOrDefault();
            long photoshopMemory = 0;
            if (photoshop != null)
                photoshopMemory = photoshop.WorkingSet64;

            // Format the data size strings. Show GB/MB/KB rather than bytes.
            string wpfMemoryString = FileSystemHelper.FormatDataSizeString(wpfMemory);
            string photoshopMemoryString = "";
            if (photoshopMemory > 0)
                photoshopMemoryString = FileSystemHelper.FormatDataSizeString(photoshopMemory);

            string finalMemoryString = "Memory Usage: " + wpfMemoryString;
            if (!string.IsNullOrEmpty(photoshopMemoryString))
                finalMemoryString += "  (" + photoshopMemoryString + " Photoshop)";

            // If we're not paused or aborting, display the memory usage in the
            // "Status" area. Paused/Aborting statuses take precedence over memory
            // usage, so don't upate the status label if we're in one of those states.
            if (!MainWindowViewModel.IsPaused && !MainWindowViewModel.IsAborting)
                MainWindowViewModel.Status = finalMemoryString;
        }

        /// <summary>
        /// Checks to see if the pause button has been pressed by the user. 
        /// If it has, it puts the worker thread to sleep and waits for the 
        /// resume button to be pressed.
        /// </summary>
        private void CheckForPause()
        {
            // If we are paused, check every second or so to see if we 
            // should resume our work.
            while (MainWindowViewModel.IsPaused && !MainWindowViewModel.IsAborting)
            {
                // If timers are already stopped, nothing happens
                StopTimers();

                // Make sure photoshop is always visible when paused. If we've set
                // it to be hidden while building, we want to re-show it now.
                PhotoshopCalls.ShowPhotoshop();

                // Sleep for a little bit, and then check again to see if
                // we're unpaused.
                Thread.Sleep(500);

                // Update the status after the short reading delay. This way,
                // the user always has time to see the longer "pausing... " 
                // message before switching text to "PAUSED".
                MainWindowViewModel.Status = "PAUSED";
            }

            // If we're done pausing, and we're not aborting, we can set photoshop
            // to hidden again if desired.
            if (!MainWindowViewModel.IsAborting && MainWindowViewModel.HidePhotoshopWhileBuilding)
                PhotoshopCalls.HidePhotoshop();

            // Re-start our timers
            StartTimers();

            // Remove the "PAUSED" status message
            if (MainWindowViewModel.Status.Equals("PAUSED"))
                MainWindowViewModel.Status = "";
        }

        /// <summary>
        /// Starts our GUI timer and the calculateStatistics timer.
        /// </summary>
        private void StartTimers()
        {
            if (!_timer.IsEnabled)
            {
                _timer.Start();
                _calculateStatisticsTimer.Start();
            }
        }

        /// <summary>
        /// Stops our GUI timer and the calculateStatistics timer.
        /// </summary>
        private void StopTimers()
        {
            if (_timer.IsEnabled)
            {
                _timer.Stop();
                _calculateStatisticsTimer.Stop();
            }
        }

        /// <summary>
        /// Should be called before the build begins. This resets all progress bar
        /// and view model values, starts our timers, etc.
        /// </summary>
        private void BuildStarted()
        {
            // Reset everything
            MainWindowViewModel.Reset();
            ProgressBars.Reset(true);

            // Larrity can start animating
            LarrityImage.Play();

            // Start our timers
            StartTimers();
            MainWindowViewModel.IsExecuting = true;
            MainWindowViewModel.HasPhotoshopBeenInitialized = false;
        }

        /// <summary>
        /// Code that we always want to run after our build has completed or
        /// been aborted.
        /// </summary>
        private void BuildCompleted()
        {
            // Close photoshop if desired
            PhotoshopCalls.FinalCleanup(MainWindowViewModel.KeepPhotoshopOpen);

            // Hide our progress bars
            UI(() => ProgressBars.Visibility = Visibility.Collapsed);

            // Update our build result label
            MainWindowViewModel.BuildResult = MainWindowViewModel.IsAborting ? "Build Aborted" : "Build Successful";

            // Stop our timers
            StopTimers();
            MainWindowViewModel.IsExecuting = false;

            // Larrity can stop now
            UI(() => LarrityImage.Pause());

            // If the user tried to close the application while the worker thread
            // was executing, we will have cancelled that operation. We didn't
            // want to close the window until the worker thread was completed.
            // Now that we're done, we want to close the app, like the user 
            // previously requested.
            if (MainWindowViewModel.IsClosing)
                UI(() => Application.Current.Shutdown());
        }

        /// <summary>
        /// Executes the provided delegate on the UIDispatcher thread. The code
        /// used to call this function is pretty damn sexy and compact. Check out
        /// these discussions if you want to read more about how to call delegates
        /// using lamda expressions and anonymous functions.
        /// http://stackoverflow.com/questions/4621623/wpf-multithreading-ui-dispatcher-in-mvvm
        /// http://stackoverflow.com/questions/2082615/pass-method-as-parameter-using-c-sharp
        /// </summary>
        /// <param name="delegateAction"></param>
        private void UI(Action delegateAction)
        {
            MainWindowViewModel.UIDispatcher.Invoke(DispatcherPriority.Normal, delegateAction);
        }

        /// <summary>
        /// Occurs when either the "Resume" button or the "Pause" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePlayPauseCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Don't start or stop the timers here. Let the worker thread do that.
            // When we hit pause, the worker is not paused immediately. It waits
            // until the current operation is completed. 
            if (MainWindowViewModel.IsPaused)
            {
                MainWindowViewModel.IsPaused = false;
                MainWindowViewModel.Status = "";
                LarrityImage.Play();
            }
            else
            {
                MainWindowViewModel.IsPaused = true;
                MainWindowViewModel.Status = "Pausing... Waiting for current operation to complete...";
                LarrityImage.Pause();
            }
        }

        /// <summary>
        /// Whether or not the "Pause/Resume" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePlayPauseCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Only enable the pause/resume buttons if we're actually executing a crawl.
            // Also, don't allow the user to pause until photoshop has been initialized.
            if (MainWindowViewModel != null && MainWindowViewModel.IsExecuting &&
                !MainWindowViewModel.IsAborting && MainWindowViewModel.HasPhotoshopBeenInitialized)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        /// <summary>
        /// Occurs when the "Abort" button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Tell the thread to gracefully stop itself if it is running
            MainWindowViewModel.IsAborting = true;

            // If we're already paused, we know the current operation 
            // is already completed, because pause waits for the current
            // operation to complete as well.
            if (!MainWindowViewModel.IsPaused)
                MainWindowViewModel.Status = "Aborting... Waiting for current operation to complete...";
        }

        /// <summary>
        /// Whether or not the "Abort" button can be clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            // Only enable the abort button if we're actually executing a crawl.
            if (MainWindowViewModel != null && MainWindowViewModel.IsExecuting && !MainWindowViewModel.IsAborting)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        /// <summary>
        /// Called every time our build progress timer fires.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            MainWindowViewModel.SecondsElapsed++;
        }

        /// <summary>
        /// Called every time our statistics timer fires.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CalculateStatisticsTimer_Tick(object sender, EventArgs e)
        {
            CalculateStatistics();
        }

        /// <summary>
        /// Handles our "OK" button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindowViewModel.IsAdornerVisible = false;
        }
    }
}