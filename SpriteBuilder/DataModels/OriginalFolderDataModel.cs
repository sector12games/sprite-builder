﻿// -----------------------------------------------------------------------
// <copyright file="OriginalFolderDataModel.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.DataModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using Sector12Common.Helpers;
    using SpriteBuilder.Logic;

    /// <summary>
    /// Class definition for OriginalFolderDataModel.cs
    /// </summary>
    public class OriginalFolderDataModel : INotifyPropertyChanged
    {
        private string _rootDirectory;
        private bool _isChecked;

        /// <summary>
        /// Initializes a new instance of the <see cref="OriginalFolderDataModel"/> class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="settingsPropertyName"></param>
        public OriginalFolderDataModel(string name, string settingsPropertyName)
        {
            Name = name;
            SettingsPropertyName = settingsPropertyName;
            BuildTasksList = new List<BuildTasks>();

            // Default all directories to checked by default
            IsChecked = true;

            // Attempt to load the RootDirectory that was used the last time
            // this program was run.
            LoadSettingsProperty();
        }

        /// <summary>
        /// The PropertyChanged event for our current WPF application.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the name/description that will be displayed in the UI 
        /// for this element. For example, "NPCs Directory".
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets the name that will be used to store the directory location 
        /// in the Visual Studio Properties.Settings.Default. Typically, we will do
        /// this manually, but because numerous directory locations could/will
        /// be added in the future, I wanted to automate 
        /// </para>
        /// <para>
        /// Example: "NpcsDirectoryLocation"
        /// </para>
        /// </summary>
        public string SettingsPropertyName { get; set; }

        /// <summary>
        /// Gets or sets a list of relative path names for every image contained 
        /// in this folder, and its subfolders. There will be one entry for each
        /// image. This will be used to determine which images need to be
        /// created or overwritten.
        /// </summary>
        public List<string> ImageRelativePathNames { get; set; }

        /// <summary>
        /// Gets or sets the list of all build tasks for each effect. This is 
        /// the list of all images that we must create and/or overwrite for 
        /// each effect.
        /// </summary>
        public List<BuildTasks> BuildTasksList { get; set; }

        /// <summary>
        /// Gets or sets the absolute path to the parent directory of this 
        /// "original" folder. This value is set by the UI when the user selects 
        /// a valid directory.
        /// </summary>
        public string RootDirectory
        {
            get
            {
                return _rootDirectory;
            }

            set
            {
                if (value != null)
                    value = value.Trim();

                _rootDirectory = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("RootDirectory");
                OnPropertyChanged("IsRootDirectoryValid");
                OnPropertyChanged("ErrorMessage");
                OnPropertyChanged("HasErrors");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not this "original" directory 
        /// will be included in the upcoming build. If this is not checked, no effect 
        /// images will be created for any of the items in this folder.
        /// </summary>
        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }

            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the directory actually exists.
        /// </summary>
        public bool IsRootDirectoryValid
        {
            get
            {
                return FileSystemHelper.IsDirectoryValid(RootDirectory);
            }
        }

        /// <summary>
        /// Gets the total number of images we need to create for this original
        /// folder, for all effects. This does not include the number of images
        /// we will have to overwrite.
        /// </summary>
        public int ImagesToCreate
        {
            get
            {
                if (BuildTasksList == null)
                    return 0;

                return BuildTasksList.Sum(tasks => tasks.ImagesToCreate.Count());
            }
        }

        /// <summary>
        /// Gets the total number of images we need to delete/overwrite for this 
        /// original folder, for all effects.
        /// </summary>
        public int ImagesToOverwrite
        {
            get
            {
                if (BuildTasksList == null)
                    return 0;

                return BuildTasksList.Sum(tasks => tasks.ImagesToOverwrite.Count());
            }
        }

        /// <summary>
        /// Gets the total number of outdated images that exist across all effect
        /// directories for this original folder. Outdated images are effect
        /// images that no longer have an original (orphans). These images
        /// can be deleted during the build if the user checks the appropriate
        /// checkbox in the UI.
        /// </summary>
        public int OutdatedImages
        {
            get
            {
                if (BuildTasksList == null)
                    return 0;

                return BuildTasksList.Sum(tasks => tasks.OutdatedImages.Count());
            }
        }

        /// <summary>
        /// Gets an error message string if there are any problems with the directory.
        /// If there are no errors, the string returned is empty.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                // Verify that the root directory provided by the user is valid
                if (!IsRootDirectoryValid)
                    return "Directory is invalid or does not exist.";

                // is indeed valid, verify that the root directory contains an 
                // 'Originals' subdirectory.
                DirectoryInfo dir = new DirectoryInfo(RootDirectory);
                if (!dir.GetDirectories(Globals.OriginalsDirectory).Any())
                    return "Directory does not contain a subdirectory named '" + Globals.OriginalsDirectory + "'.";

                // No error message, the directory is valid
                return "";
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not this original folder has any errors.
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return ErrorMessage.Any();
            }
        }

        /// <summary>
        /// <para>
        /// Save the RootDirectory location for use next time the application
        /// runs. In order to use this, you need to add a setting manually in
        /// Properties/Settings.Settings. 
        /// </para>
        /// <para>
        /// I've tried automating this process, but I've attempted all of the
        /// ideas I've come across, and they all have failed. All of the ideas
        /// look like they are not directly supported by Microsoft documentation,
        /// and are more like hacks. Even if a solution works in VS 2010, it might
        /// not work in newer versions. The only valid idea I've seen is Tom Wilson's
        /// solution in the following article (splitting a collection):
        /// http://stackoverflow.com/questions/175726/c-create-new-settings-at-run-time
        /// </para>
        /// </summary>
        public void SaveSettingsProperty()
        {
            Properties.Settings.Default[SettingsPropertyName] = RootDirectory;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Loads the RootDirectory value the user selected the last time this 
        /// program was run (if it exists).
        /// </summary>
        public void LoadSettingsProperty()
        {
            RootDirectory = (string)Properties.Settings.Default[SettingsPropertyName];
        }

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event.
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}