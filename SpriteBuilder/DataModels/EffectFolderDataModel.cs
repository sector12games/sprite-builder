﻿// -----------------------------------------------------------------------
// <copyright file="EffectFolderDataModel.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.DataModels
{
    using System.Collections.Generic;

    /// <summary>
    /// Class definition for EffectFolderDataModel.cs.
    /// </summary>
    public class EffectFolderDataModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EffectFolderDataModel"/> class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="folderName"></param>
        /// <param name="photoshopActionName"></param>
        public EffectFolderDataModel(string name, string folderName, string photoshopActionName)
        {
            Name = name;
            FolderName = folderName;
            PhotoshopActionName = photoshopActionName;

            // Check all effects by default
            IsChecked = true;
        }

        /// <summary>
        /// <para>
        /// Gets or sets the name/description that will be displayed in the UI 
        /// for this element. This will typically be almost the same as the 
        /// FolderName, but will probably include spaces. 
        /// </para>
        /// <para>
        /// For example, "Brush Stroke". 
        /// </para>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the directory for this effect. All images 
        /// created for this effect will be stored in this directory.
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// Gets or sets the name of the action that will be executed on each
        /// original image in photoshop. These actions are created manually
        /// in photoshop, and stored in Sector12/Content/
        /// </summary>
        public string PhotoshopActionName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not images for this effect 
        /// will be created in the upcoming build. 
        /// </summary>
        public bool IsChecked { get; set; }

        /// <summary>
        /// Gets or sets a list of relative path names for every image contained 
        /// in this folder, and its subfolders. There will be one entry for each
        /// image. This will be used to determine which images need to be
        /// created or overwritten.
        /// </summary>
        public List<string> ImageRelativePathNames { get; set; }
    }
}