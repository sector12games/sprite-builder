﻿// -----------------------------------------------------------------------
// <copyright file="MainWindowViewModel.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using System.Windows.Threading;
    using SpriteBuilder.DataModels;
    using SpriteBuilder.Logic;

    /// <summary>
    /// Class definition for MainWindowViewModel.cs
    /// </summary>
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private bool _isAdornerVisible;
        private string _ignoreFolders;
        private bool _overwrite;
        private string _buildResult;
        private bool _deleteOutdatedEffectImages;
        private bool _hidePhotoshopWhileBuilding;
        private bool _useDelayForPhotoshopResponsiveness;
        private bool _keepPhotoshopOpen;
        private bool _disableProgressBarReadabilityDelay;
        private bool _isPaused;
        private int _secondsElapsed;
        private string _status;
        private string _errorStatusLabel;
        private bool _isAborting;
        private bool _isExecuting;
        private bool _isClosing;
        private bool _hasPhotoshopBeenInitialized;
        private int _imagesCreated;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class. 
        /// </summary>
        public MainWindowViewModel()
        {
            // Set default checkbox values
            _disableProgressBarReadabilityDelay = false;
            _useDelayForPhotoshopResponsiveness = false;
            _hidePhotoshopWhileBuilding = false;
            _keepPhotoshopOpen = true;
            _overwrite = false;

            // Create the list of all original images, that we will want to perform
            // effect logic on. For example, we'll want the "NPCs" folder, the 
            // "Objects" folder, and potentially other folders like "Monsters",
            // "Mercenaries", etc. in the future. 
            OriginalFolders = new ObservableCollection<OriginalFolderDataModel>
            {
                new OriginalFolderDataModel("NPCs Directory:", "NpcsDirectoryLocation"),
                new OriginalFolderDataModel("Objects Directory:", "ObjectsDirectoryLocation")
            };

            // Create the list of all effects. Each effect will be stored in it's own
            // separate folder.
            EffectFolders = new ObservableCollection<EffectFolderDataModel>
            {
                new EffectFolderDataModel("Brush Stroke", "BrushStroke", "Angled Strokes Sprites"),
                new EffectFolderDataModel("Transparency", "Transparency", "Sprite Transparency Effect")
            };

            // Load the IgnoreDirectories string that was used last time this program
            // was run.
            IgnoreFolders = Properties.Settings.Default.IgnoreFolders;
        }

        /// <summary>
        /// The PropertyChanged event for our current WPF application.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the UIDispatcher thread for our current WPF application.
        /// </summary>
        public Dispatcher UIDispatcher { get; set; }

        /// <summary>
        /// <para>
        /// Gets or sets a list of "originals" folders. These are the folder 
        /// locations where all of the original, unaltered game images appear. 
        /// There may be many of these directories (NPCs, Objects, Monsters, 
        /// Mercenaries, etc).
        /// </para>
        /// <para>
        /// All of these directory locations are set in the UI by the user.
        /// </para>
        /// </summary>
        public ObservableCollection<OriginalFolderDataModel> OriginalFolders { get; set; }

        /// <summary>
        /// Gets or sets a list of "effect" folders. Each effect that we build 
        /// (Transparency, BrushStroke, etc) will store all of it's images in 
        /// a new folder.
        /// </summary>
        public ObservableCollection<EffectFolderDataModel> EffectFolders { get; set; }

        /// <summary>
        /// Gets or sets a new-line delimited list of directories to ignore during 
        /// the build.
        /// </summary>
        public string IgnoreFolders
        {
            get
            {
                return _ignoreFolders;
            }

            set
            {
                _ignoreFolders = value;

                // We also need to correctly format our string
                _ignoreFolders = FormatIgnoreFoldersString(_ignoreFolders);

                OnPropertyChanged("IgnoreFolders");
            }
        }

        /// <summary>
        /// Gets or sets data regarding folders and images that are ignored via the
        /// "Ignore Directories" option in the UI.
        /// </summary>
        public IgnoredFolderData IgnoredFolderData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not our ProgressOverlay is 
        /// currently visible.
        /// </summary>
        public bool IsAdornerVisible
        {
            get
            {
                return _isAdornerVisible;
            }

            set
            {
                _isAdornerVisible = value;
                OnPropertyChanged("IsAdornerVisible");
            }
        }

        /// <summary>
        /// Gets the total number of fresh images to create for this build.
        /// This number does not include images that will be overwritten
        /// when they are created.
        /// </summary>
        public int ImagesToCreate
        {
            get
            {
                return GetCheckedOriginalFolders().Sum(o => o.ImagesToCreate);
            }
        }

        /// <summary>
        /// Gets the total number of outdated images that exist across all effect
        /// directories for all original folders. Outdated images are effect
        /// images that no longer have an original (orphans). These images
        /// can be deleted during the build if the user checks the appropriate
        /// checkbox in the UI.
        /// </summary>
        public int OutdatedImages
        {
            get
            {
                return GetCheckedOriginalFolders().Sum(o => o.OutdatedImages);
            }
        }

        /// <summary>
        /// Gets the total number of images that will be overwritten during the
        /// upcoming build.
        /// </summary>
        public int ImagesToOverwrite
        {
            get
            {
                return GetCheckedOriginalFolders().Sum(o => o.ImagesToOverwrite);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to overwrite old files 
        /// when building zones.
        /// </summary>
        public bool Overwrite
        {
            get
            {
                return _overwrite;
            }

            set
            {
                _overwrite = value;
                OnPropertyChanged("Overwrite");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to delete previously 
        /// created effect images that no longer have a matching original 
        /// (orphaned effect images). This can occur when an effect image was 
        /// previously created, but the original has since been deleted.
        /// </summary>
        public bool DeleteOutdatedEffectImages
        {
            get
            {
                return _deleteOutdatedEffectImages;
            }

            set
            {
                _deleteOutdatedEffectImages = value;
                OnPropertyChanged("DeleteOutdatedEffectImages");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to hide photoshop while 
        /// executing the build. This will hide it and remove it from the 
        /// task bar.
        /// </summary>
        public bool HidePhotoshopWhileBuilding
        {
            get
            {
                return _hidePhotoshopWhileBuilding;
            }

            set
            {
                _hidePhotoshopWhileBuilding = value;
                OnPropertyChanged("HidePhotoshopWhileBuilding");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to use a small delay 
        /// between commands to prevent photoshop from choking and becoming 
        /// unresponsive.
        /// </summary>
        public bool UseDelayForPhotoshopResponsiveness
        {
            get
            {
                return _useDelayForPhotoshopResponsiveness;
            }

            set
            {
                _useDelayForPhotoshopResponsiveness = value;
                OnPropertyChanged("UseDelayForPhotoshopResponsiveness");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to keep photoshop open 
        /// after the font building process has been completed.
        /// </summary>
        public bool KeepPhotoshopOpen
        {
            get
            {
                return _keepPhotoshopOpen;
            }

            set
            {
                _keepPhotoshopOpen = value;
                OnPropertyChanged("KeepPhotoshopOpen");
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not to disable a readability 
        /// delay that I typically put in all of my WPF applications so that I have 
        /// time to read what the progress bars say before they change themselves 
        /// again. 
        /// </para>
        /// <para>
        /// The delay is only about a second or so, and does not make a significant
        /// impact when building individual zones. However, disabling the delay can 
        /// save quite a few minutes if we are doing a batch build, so I have included
        /// the option to turn it off.
        /// </para>
        /// </summary>
        public bool DisableProgressBarReadabilityDelay
        {
            get
            {
                return _disableProgressBarReadabilityDelay;
            }

            set
            {
                _disableProgressBarReadabilityDelay = value;
                OnPropertyChanged("DisableProgressBarReadabilityDelay");
            }
        }

        /// <summary>
        /// <para>Gets the current play/pause button text.</para>
        /// <para>
        /// When paused, the play/pause button should display "Play". When executing,
        /// the play/pause button should display "Pause".
        /// </para>
        /// </summary>
        public string PlayPauseButtonText
        {
            get
            {
                if (IsPaused)
                    return "Resume";
                else
                    return "Pause";
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the font build process 
        /// is currently paused.
        /// </summary>
        public bool IsPaused
        {
            get
            {
                return _isPaused;
            }

            set
            {
                _isPaused = value;
                OnPropertyChanged("IsPaused");
                OnPropertyChanged("PlayPauseButtonText");
            }
        }

        /// <summary>
        /// Gets or sets the total number of images that have been created thus far.
        /// </summary>
        public int ImagesCreated
        {
            get
            {
                return _imagesCreated;
            }

            set
            {
                _imagesCreated = value;
                OnPropertyChanged("ImagesCreated");
                OnPropertyChanged("ImagesCreatedLabelString");
            }
        }

        /// <summary>
        /// Gets a nicely formatted string for the UI.
        /// </summary>
        public string ImagesCreatedLabelString
        {
            get
            {
                return ImagesCreated + " images created.";
            }
        }

        /// <summary>
        /// Gets or sets the total number of seconds elapsed during this build.
        /// </summary>
        public int SecondsElapsed
        {
            get
            {
                return _secondsElapsed;
            }

            set
            {
                _secondsElapsed = value;
                OnPropertyChanged("SecondsElapsed");
                OnPropertyChanged("TimerText");
                OnPropertyChanged("TimerTextLong");
            }
        }

        /// <summary>
        /// Gets the total number of seconds elapsed as a nicely formatted 
        /// timer string.
        /// </summary>
        public string TimerText
        {
            get
            {
                // Convert the seconds elapsed to a timespan for easy string formatting
                TimeSpan elapsed = new TimeSpan(_secondsElapsed * TimeSpan.TicksPerSecond);

                // Only display the hours if necessary. Always display the minutes and seconds
                if (elapsed.Hours > 0)
                    return string.Format(Globals.MoreThanOneHourDateFormat, elapsed);
                else
                    return string.Format(Globals.LessThanOneHourDateFormat, elapsed);
            }
        }

        /// <summary>
        /// Gets an even more verbose method of displaying the total seconds elapsed 
        /// to the user.
        /// </summary>
        public string TimerTextLong
        {
            get
            {
                return "Total Time Elapsed: " + TimerText;
            }
        }

        /// <summary>
        /// Gets or sets the current status of the build process. This could be a 
        /// variety of conditions - PAUSED, Pausing..., Aborting..., etc).
        /// </summary>
        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        /// <summary>
        /// Gets or sets the build result. This is displayed to the user after 
        /// completion. Should be something along the lines of "Build Successful",
        /// "Failed", or "Aborted".
        /// </summary>
        public string BuildResult
        {
            get
            {
                return _buildResult;
            }

            set
            {
                _buildResult = value;
                OnPropertyChanged("BuildResult");
            }
        }

        /// <summary>
        /// Gets or sets a small error message that is shown next to the "Build"
        /// button on the UI. If there are any problems with the directories
        /// selected, or the user has not selected any effects to build, a
        /// message will be displayed to help him fix the problem.
        /// </summary>
        public string ErrorStatusLabel
        {
            get
            {
                return _errorStatusLabel;
            }

            set
            {
                _errorStatusLabel = value;
                OnPropertyChanged("ErrorStatusLabel");
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not our build is currently
        /// attempting to abort.
        /// </para>
        /// <para>
        /// Our worker thread will attempt to gracefully stop when the Abort button
        /// is clicked. This means it will finish it's current task, and stop 
        /// immediately after. If it is in the process of executing an external 
        /// photoshop script, this could take several seconds.
        /// </para>
        /// <para>
        /// While we are in the process of aborting, all buttons need to be disabled.
        /// </para>
        /// </summary>
        public bool IsAborting
        {
            get
            {
                return _isAborting;
            }

            set
            {
                _isAborting = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("IsAborting");

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands();
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether or not our application is 
        /// executing.
        /// </para>
        /// <para>
        /// If the application is paused, IsExecuting will still return true. 
        /// If the user clicks abort, and a task is still running for a few seconds
        /// afterwards, IsExecuting will still return true until the task is finished 
        /// and the thread is dead.
        /// </para>
        /// </summary>
        public bool IsExecuting
        {
            get
            {
                return _isExecuting;
            }

            set
            {
                _isExecuting = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("IsExecuting");

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the close/exit button have
        /// been pressed. This is needed by the worker thread. It lets the worker 
        /// thread know that it shouldn't update any UI elements on the UIDispatcher 
        /// thread anymore. If we attempt to do something like set our progress bars 
        /// to invisible while the window is in a "closing" situation, the application
        /// will hang.
        /// </summary>
        public bool IsClosing
        {
            get
            {
                return _isClosing;
            }

            set
            {
                _isClosing = value;
                OnPropertyChanged("IsClosing");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not photoshop has been 
        /// initialized, and any up-front loading logic has been performed. 
        /// We do not allow the user to pause the build until after photoshop 
        /// has at least been loaded, and the real build has been started.
        /// </summary>
        public bool HasPhotoshopBeenInitialized
        {
            get
            {
                return _hasPhotoshopBeenInitialized;
            }

            set
            {
                _hasPhotoshopBeenInitialized = value;

                // Notify WPF that the property changed, so any GUI elements bound
                // to it will update themselves.
                OnPropertyChanged("HasPhotoshopBeenInitialized");

                // Tell all commands to recheck their canExecute methods. 
                RefreshCommands();
            }
        }

        /// <summary>
        /// Returns a list of all checked "original" folders.
        /// </summary>
        /// <returns>A list of all checked "original" folders.</returns>
        public IEnumerable<OriginalFolderDataModel> GetCheckedOriginalFolders()
        {
            return OriginalFolders.Where(x => x.IsChecked);
        }

        /// <summary>
        /// Returns a list of all checked "effect" folders.
        /// </summary>
        /// <returns>A list of all checked "effect" folders.</returns>
        public IEnumerable<EffectFolderDataModel> GetCheckedEffectFolders()
        {
            return EffectFolders.Where(x => x.IsChecked);
        }

        /// <summary>
        /// Whether or not at least one "original" folder has been selected in the UI.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasCheckedOriginalFolder()
        {
            if (GetCheckedOriginalFolders().Any())
                return true;
            else
                return false;
        }

        /// <summary>
        /// Whether or not at least one "effect" folder has been selected in the UI.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasCheckedEffectFolder()
        {
            if (GetCheckedEffectFolders().Any())
                return true;
            else
                return false;
        }

        /// <summary>
        /// Reset all variables to their default values. This will get
        /// called whenever the user clicks the build button to start a
        /// new operation.
        /// </summary>
        public void Reset()
        {
            IsAborting = false;
            IsPaused = false;
            Status = "";
            SecondsElapsed = 0;
            ImagesCreated = 0;
        }

        /// <summary>
        /// Correctly format the "Ignore Directories" string. Each ignore pattern
        /// needs to be trimmed and have a single backslash at the start and end.
        /// </summary>
        /// <param name="ignoreFoldersString"></param>
        /// <returns>
        /// The formatted <see cref="string"/>.
        /// </returns>
        public string FormatIgnoreFoldersString(string ignoreFoldersString)
        {
            // Create a stringbuilder we will use to format and rebuild the 
            // IgnoreFolders string.
            StringBuilder sb = new StringBuilder();

            // First, trim the entire string
            ignoreFoldersString = ignoreFoldersString.Trim();

            // Now, split the list. Remember that the list is new-line separated. 
            string[] newLineDelimiter = { Environment.NewLine };
            string[] ignoreStrings = ignoreFoldersString.Split(
                newLineDelimiter,
                StringSplitOptions.None);

            // Now we need to trim and format each individual ignore pattern
            foreach (string s in ignoreStrings)
            {
                // Trim the single folder entry
                string newString = s.Trim();

                // Don't do anything more if our string is empty
                if (string.IsNullOrEmpty(newString))
                    continue;

                // Add a backslash to the front and back of the string
                newString = "\\" + newString + "\\";

                // As a convenience, replace all forward slashes with backslashes
                Regex rgx = new Regex(@"/+"); // forward slash repeated any number of times)
                newString = rgx.Replace(newString, @"\");

                // We now know that the string contains at least one backslash
                // at the beginning and end of the pattern. We now need to make
                // sure that there are not more than 1 backslashes in a row,
                // at the beginning, end, or anywhere in the middle of the string.
                rgx = new Regex(@"\\+"); // (backslash repeated any number of times)
                newString = rgx.Replace(newString, @"\"); // (replace any number of backslashes in a row with a single)

                // Add the newly formatted string to the string builder
                sb.AppendLine(newString);
            }

            // Now that all folder entries have been formatted, return the 
            // result. Add one more trim to remove the newline character 
            // added by our last item in the list.
            return sb.ToString().Trim();
        }

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event.
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Refreshes the state of all WPF commands.
        /// </summary>
        private void RefreshCommands()
        {
            // Make sure to call it on the UI thread, or it won't work.
            UIDispatcher.Invoke(
                DispatcherPriority.Normal,
                (Action)CommandManager.InvalidateRequerySuggested);
        }
    }
}