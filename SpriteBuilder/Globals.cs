﻿// -----------------------------------------------------------------------
// <copyright file="Globals.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder
{
    using System.Collections.Generic;

    /// <summary>
    /// Contains all of our project's global variables.
    /// </summary>
    public static class Globals
    {
        /// <summary>
        /// When displaying a graphical timer, I always want to show the
        /// minutes and seconds, but I don't want to show the hours unless
        /// they are necessary. This isn't possible using a single format
        /// string as far as I can tell, so I have to use two.
        /// Examples:
        /// 00:15       (15 secs - don't show hours)
        /// 10:15       (10 min, 15 secs - don't show hours)
        /// 01:14:22    (1 hour, 14 min, 22 secs)
        /// </summary>
        public const string LessThanOneHourDateFormat = "{0:mm\\:ss}";

        public const string MoreThanOneHourDateFormat = "{0:hh\\:mm\\:ss}";

        /// <summary>
        /// How often to calculate memory/diskspace statistics.
        /// </summary>
        public const int CalculateStatisticsFrequency = 500;

        /// <summary>
        /// A delay used to ensure the user gets to see graphical status 
        /// messages before they dissapear.
        /// </summary>
        public const int ReadingDelay = 1000;

        /// <summary>
        /// A small delay before photoshop commands to ensure that photoshop
        /// does not choke and become unresponsive. You can turn this option
        /// off by unchecking a value on the user interface.
        /// </summary>
        public const int PhotoshopResponsivenessDelay = 200;

        /// <summary>
        /// The name of the "Originals" directory that is required to be in
        /// every build directory.
        /// </summary>
        public const string OriginalsDirectory = "Originals";

        /// <summary>
        /// A list of all image types that the sprite builder will scan for.
        /// If an original image does not have one of these file extensions,
        /// effect sprites will not be built for it.
        /// </summary>
        public static readonly List<string> ValidImageExtensions = new List<string>
        {
            ".png",
            ".jpg",
            ".gif"
        };
    }
}