﻿// -----------------------------------------------------------------------
// <copyright file="BuildTasks.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.Logic
{
    using System.Collections.Generic;
    using SpriteBuilder.DataModels;

    /// <summary>
    /// Class definition for BuildTasks.cs
    /// </summary>
    public class BuildTasks
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BuildTasks"/> class.
        /// </summary>
        /// <param name="original"></param>
        /// <param name="effect"></param>
        public BuildTasks(OriginalFolderDataModel original, EffectFolderDataModel effect)
        {
            OriginalFolder = original;
            EffectFolder = effect;

            ImagesToCreate = new List<string>();
            ImagesToOverwrite = new List<string>();
            OutdatedImages = new List<string>();
        }

        /// <summary>
        /// Gets or sets the original folder that these build task pertain to.
        /// </summary>
        public OriginalFolderDataModel OriginalFolder { get; set; }

        /// <summary>
        /// Gets or sets the effect folder that these build tasks pertain to.
        /// </summary>
        public EffectFolderDataModel EffectFolder { get; set; }

        /// <summary>
        /// Gets or sets the list of relative image paths that need to be created. 
        /// This does not include images that need to be overwritten
        /// </summary>
        public List<string> ImagesToCreate { get; set; }

        /// <summary>
        /// Gets or sets the list of releative image paths that will be overwritten
        /// (deleted before the build starts).
        /// </summary>
        public List<string> ImagesToOverwrite { get; set; }

        /// <summary>
        /// Gets or sets a list of orphaned effect images that no longer have an 
        /// original. This can occasionally happen if an effect images was 
        /// previously generated, and the original image has since been deleted.
        /// </summary>
        public List<string> OutdatedImages { get; set; }
    }
}