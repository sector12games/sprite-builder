﻿// -----------------------------------------------------------------------
// <copyright file="IgnoredFolderData.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.Logic
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Class definition for IgnoredFolderData.cs
    /// </summary>
    public class IgnoredFolderData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IgnoredFolderData"/> class.
        /// </summary>
        public IgnoredFolderData()
        {
            IgnoredFolders = new List<DirectoryInfo>();
            IgnoredSubdirectories = new List<DirectoryInfo>();
            IgnoredImages = new List<FileInfo>();
        }

        /// <summary>
        /// Gets or sets the list of folders are being ignored due to the 
        /// "Ignore Directories" setting in the UI. This is not including 
        /// any of the folders' subdirectories. It only counts folders 
        /// that exactly match the "Ignore Directories" strings.
        /// </summary>
        public List<DirectoryInfo> IgnoredFolders { get; set; }

        /// <summary>
        /// Gets or sets the list of subfolders that are being ignored 
        /// due to the "Ignore Directories" setting in the UI.
        /// </summary>
        public List<DirectoryInfo> IgnoredSubdirectories { get; set; }

        /// <summary>
        /// Gets or sets the list of all images being ignored due to the 
        /// "Ignore Directories" setting in the UI. This only counts valid 
        /// images that would normally be processed during the build. This 
        /// includes all images in all subdirectories.
        /// </summary>
        public List<FileInfo> IgnoredImages { get; set; }
    }
}