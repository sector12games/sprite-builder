﻿// -----------------------------------------------------------------------
// <copyright file="FolderPrep.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Sector12Common.ExtensionMethods;
    using SpriteBuilder.DataModels;

    /// <summary>
    /// Class definition for FolderPrep.cs
    /// </summary>
    public static class FolderPrep
    {
        /// <summary>
        /// <para>
        /// Returns a list of all the ignored directories, subdirectories,
        /// and image files. These are all images that are ignored because
        /// of the "Ignore Directories" filter that is set in the UI.
        /// </para>
        /// <para>
        /// This is the list of all "original" directories and images that
        /// are being ignored. Any of their corresponding effect directories
        /// and images will also be ignored by the build process, but these
        /// items are not added to the IgnoredFolderData object.
        /// </para>
        /// </summary>
        /// <param name="checkedOriginalFolders"></param>
        /// <param name="ignoreFolders"></param>
        /// <returns>
        /// The <see cref="IgnoredFolderData"/>. Returns an empty object if 
        /// no folders are being ignored.
        /// </returns>
        public static IgnoredFolderData GetIgnoredFolderData(
            IEnumerable<OriginalFolderDataModel> checkedOriginalFolders,
            string ignoreFolders)
        {
            IgnoredFolderData data = new IgnoredFolderData();

            // Iterate over all checked "original" folders to determine which
            // directories we will ignore. Don't worry about files yet.
            foreach (OriginalFolderDataModel o in checkedOriginalFolders)
            {
                // Check to see if the directory, or any of it's subdirectories
                // are in the ignore list.
                DirectoryInfo dir = new DirectoryInfo(o.RootDirectory + "\\" + Globals.OriginalsDirectory);
                GetIgnoredFolderDataRecursive(dir, dir, ignoreFolders, data);
            }

            // Now that we have our full list of ignored directories, count how
            // many valid image files and subdirectories each one has. Don't count 
            // non-images in this list. Only count valid image files that are filtered
            // out because of the "Ignore Directories" filter in the UI.
            foreach (DirectoryInfo ignoredDir in data.IgnoredFolders)
            {
                CountSubdirectoriesAndFilesInIgnoredDirectory(ignoredDir, data);
            }

            return data;
        }

        /// <summary>
        /// Scans each original and original/effect folder combo for all images.
        /// Determines all images that need to be created and/or deleted for each
        /// original/effect folder pair. The results are stored in each original's
        /// OriginalFolderDataModel.BuildTasksList.
        /// </summary>
        /// <param name="originalFolders"></param>
        /// <param name="effectFolders"></param>
        /// <param name="ignoreFolders"></param>
        public static void DetermineBuildTasks(
            IEnumerable<OriginalFolderDataModel> originalFolders,
            IEnumerable<EffectFolderDataModel> effectFolders,
            string ignoreFolders)
        {
            // First, convert the effect folders to a list to make resharper happy.
            // Resharper will (rightly) complain because the effectFolders get 
            // enumerated on in multiple times (because we have two foreach loops).
            // If effectFolders happened to be an EntityFramework object or a 
            // LinqToSql expression that fetched data from the database, we would
            // first want to convert the IEnumerable to a list, instead of fetching
            // the same data multiple times. In our case, the effectFolders already
            // are a list, but I'm keeping this code in as an example for the future.
            // I don't want to disable the resharper rule either, because it could
            // be dangerous in the future. See here for more details:
            // http://confluence.jetbrains.com/display/ReSharper/Possible+multiple+enumeration+of+IEnumerable
            List<EffectFolderDataModel> effectFoldersIteratorSafe = effectFolders.ToList();

            foreach (OriginalFolderDataModel original in originalFolders)
            {
                // First, clear any old build tasks that might have existed from
                // a previous build attempt.
                original.BuildTasksList = new List<BuildTasks>();

                // Don't bother building tasks for folders that are not checked
                if (!original.IsChecked)
                    continue;

                // Build a list of all relative image paths for our original folder. 
                // This includes sub-directories.
                DirectoryInfo originalDir = new DirectoryInfo(
                    original.RootDirectory + "\\" + Globals.OriginalsDirectory);
                original.ImageRelativePathNames = GetRelativeImagePaths(originalDir, ignoreFolders, false);

                foreach (EffectFolderDataModel effect in effectFoldersIteratorSafe)
                {
                    // Now, build a list of all relative image paths for our effect
                    // folder. The paths are relative to the original folder.
                    DirectoryInfo effectDir = new DirectoryInfo(original.RootDirectory + "\\" + effect.FolderName);
                    effect.ImageRelativePathNames = GetRelativeImagePaths(effectDir, ignoreFolders, false);

                    // Determine all of the images we need to create and/or delete
                    // for this original/effect pair.
                    BuildTasks tasks = DetermineBuildTasks(original, effect);

                    // Add the build task to the list
                    original.BuildTasksList.Add(tasks);
                }
            }
        }

        /// <summary>
        /// The recursive call for GetIgnoredFolderData().
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="originalTopLevelDir"></param>
        /// <param name="ignoreFolders"></param>
        /// <param name="data"></param>
        private static void GetIgnoredFolderDataRecursive(
            DirectoryInfo dir,
            DirectoryInfo originalTopLevelDir,
            string ignoreFolders,
            IgnoredFolderData data)
        {
            // Check the directory to see if it is being ignored. If it is, add it
            // to the list, and don't check any of it's subdirectories. We will
            // count how many subdirectories and files it contains later.
            if (IsFolderBeingIgnored(dir, originalTopLevelDir.FullName, ignoreFolders))
            {
                data.IgnoredFolders.Add(dir);
                return;
            }

            // If the directory is not being ignored, check it's subdirectories
            foreach (DirectoryInfo subdir in dir.GetDirectories())
            {
                GetIgnoredFolderDataRecursive(subdir, originalTopLevelDir, ignoreFolders, data);
            }
        }

        /// <summary>
        /// Recursively counts all subdirectories and valid image files that are 
        /// residing within a directory the user has opted to ignore.
        /// </summary>
        /// <param name="ignoredDir"></param>
        /// <param name="data"></param>
        private static void CountSubdirectoriesAndFilesInIgnoredDirectory(
            DirectoryInfo ignoredDir,
            IgnoredFolderData data)
        {
            // Check to see how many valid image files are in this directory
            foreach (FileInfo file in ignoredDir.GetFiles())
            {
                // If the file is an image, add it's relative path to the list
                if (Globals.ValidImageExtensions.Contains(file.Extension))
                    data.IgnoredImages.Add(file);
            }

            // Now, check for any subdirectories
            foreach (DirectoryInfo subdir in ignoredDir.GetDirectories())
            {
                // Add each subdir to the list
                data.IgnoredSubdirectories.Add(subdir);

                // Now recursively check the subdirectory for files and more subdirs
                CountSubdirectoriesAndFilesInIgnoredDirectory(subdir, data);
            }
        }

        /// <summary>
        /// Determines the images to be created, overwritten, and finds any outdated
        /// effect images. All of this info is determined based on the relative image
        /// paths that were previously calculated for each original and original/effect
        /// combo folder.
        /// </summary>
        /// <param name="original"></param>
        /// <param name="effect"></param>
        /// <returns>
        /// The <see cref="BuildTasks"/>.
        /// </returns>
        private static BuildTasks DetermineBuildTasks(OriginalFolderDataModel original, EffectFolderDataModel effect)
        {
            // Create a new BuildTask for the original/effect pair
            BuildTasks result = new BuildTasks(original, effect);

            // We have already deteremined a relative path for each image in both
            // the original and the effect folders. Compare the two lists to 
            // determine which images need to be created, and which images need
            // to be overwritten.
            foreach (string path in original.ImageRelativePathNames)
            {
                // If the image exists in both the original and the effectg foler,
                // it needs to be overwritten in the build process (or ignored, if 
                // overwrite is not specified).
                if (effect.ImageRelativePathNames.Contains(path))
                    result.ImagesToOverwrite.Add(path);
                else
                    result.ImagesToCreate.Add(path);
            }

            // Next, check for outdated images. It's possible that an effect image
            // was created previously, but the original image has since been deleted.
            // Add orphaned images like this to another list, which will possibly
            // be deleted/cleaned up during the build.
            foreach (string path in effect.ImageRelativePathNames)
            {
                // If the image exists in the effect folder, but not the original 
                // folder, it is an orphaned, outdated image.
                if (!original.ImageRelativePathNames.Contains(path))
                    result.OutdatedImages.Add(path);
            }

            return result;
        }

        /// <summary>
        /// Gets the relative path to each image in the directory. All images within
        /// subdirectories are included.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="ignoreFolders"></param>
        /// <param name="includeTopDirectoryInPath"></param>
        /// <returns>
        /// A list of strings. Returns an empty list if the directory or images
        /// do not exist.
        /// </returns>
        private static List<string> GetRelativeImagePaths(
            DirectoryInfo dir,
            string ignoreFolders,
            bool includeTopDirectoryInPath)
        {
            // If the directory is not valid, return an empty list
            if (dir == null || !dir.Exists)
                return new List<string>();

            // Build the list of relative paths
            if (includeTopDirectoryInPath)
                return GetRelativeImagePathsRecursive(dir, dir, ignoreFolders, "", true);
            else
                return GetRelativeImagePathsRecursive(dir, dir, ignoreFolders, "", false);
        }

        /// <summary>
        /// The recursive call for GetRelativeImagePaths().
        /// </summary>
        /// <param name="currentDir"></param>
        /// <param name="originalTopLevelDir"></param>
        /// <param name="ignoreFolders"></param>
        /// <param name="pathPrefix"></param>
        /// <param name="includeCurrentDirInPath"></param>
        /// <returns>
        /// A list of strings. Returns an empty list if the directory or images
        /// do not exist.
        /// </returns>
        private static List<string> GetRelativeImagePathsRecursive(
            DirectoryInfo currentDir,
            DirectoryInfo originalTopLevelDir,
            string ignoreFolders,
            string pathPrefix,
            bool includeCurrentDirInPath)
        {
            List<string> result = new List<string>();

            // If the directory is in the "ignore list", don't use it, or any of 
            // its subdirectories. 
            if (IsFolderBeingIgnored(currentDir, originalTopLevelDir.FullName, ignoreFolders))
                return new List<string>();

            // Search the directory for images
            foreach (FileInfo file in currentDir.GetFiles())
            {
                // If the file is an image, add it's relative path to the list
                if (Globals.ValidImageExtensions.Contains(file.Extension))
                {
                    // Remember to add the parent directory's name to the pathPrefix. Only
                    // include the directory name if specified. It's possible we don't
                    // want to include the top directory (includeTopDirectoryInPath is set
                    // to false in the method that called this recursive method).
                    string relativePath;
                    if (includeCurrentDirInPath)
                        relativePath = pathPrefix + currentDir.Name + "\\" + file.Name;
                    else
                        relativePath = pathPrefix + file.Name;

                    result.Add(relativePath);
                }
            }

            // Now, search all subdirectories for images as well
            foreach (DirectoryInfo subdir in currentDir.GetDirectories())
            {
                // Remember to add the parent directory's name to the pathPrefix. Only
                // include the directory name if specified. It's possible we don't
                // want to include the top directory (includeTopDirectoryInPath is set
                // to false in the method that called this recursive method).
                string newPrefix;
                if (includeCurrentDirInPath)
                    newPrefix = pathPrefix + currentDir.Name + "\\";
                else
                    newPrefix = pathPrefix;

                // Now make the recursive call. All child subdirectories should be
                // included in the path. I really only wanted the option to omit
                // the top level directory from the path.
                result.AddRange(
                    GetRelativeImagePathsRecursive(
                        subdir,
                        originalTopLevelDir,
                        ignoreFolders,
                        newPrefix,
                        true));
            }

            return result;
        }

        /// <summary>
        /// <para>
        /// Checks to see if the folder passed in (or any of it's parent folders)
        /// are in the "Ignore Directories" list. Folders are matched exactly,
        /// there are no wildcards. Case is ignored.
        /// </para>
        /// <para>
        /// Not all of the directory's parents are checked. Only the parents 
        /// that reside within the originalFolder specified by the user.
        /// For example, if our original folder is "C:\Ryamar\Resources",
        /// "C:\" and "C:\Ryamar" will not be checked for ignores.
        /// </para>
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="originalFolder"></param>
        /// <param name="ignoreFolders"></param>
        /// <returns>The <see cref="bool"/>.</returns>
        private static bool IsFolderBeingIgnored(DirectoryInfo dir, string originalFolder, string ignoreFolders)
        {
            // If the ignore folders are null, we have nothing to ignore.
            if (string.IsNullOrEmpty(ignoreFolders))
                return false;

            // We want to start searching from the original directory.
            // We do not want to match strings outside of our target.
            // For example, if our target original directory is 
            // c:\Faces\Ryamar\Originals\, we only want to filter out 
            // c:\Faces\Ryamar\Originals\Female\Actor4\Faces\
            // AND NOT c:\Faces
            string pathSegmentToCheck = dir.FullName.Substring(originalFolder.Length) + "\\";

            // Split the ignore strings and path segments into individual folder 
            // names. You can't simply use \n here, because a newline character
            // in windows shows up as \r\n. Because of this, you must use 
            // Environment.NewLine expressed as an array of strings. For more info,
            // see this discussion:
            // http://stackoverflow.com/questions/1547476/easiest-way-to-split-a-string-on-newlines-in-net
            string[] newLineDelimiter = { Environment.NewLine };
            string[] ignoreStrings = ignoreFolders.Split(newLineDelimiter, StringSplitOptions.RemoveEmptyEntries);

            // Check to see if the path segment we want to check contains any of the
            // ignore patterns. If it does, this folder should be ignored.
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (string ignoreFolder in ignoreStrings)
            {
                if (pathSegmentToCheck.ContainsIgnoreCase(ignoreFolder))
                    return true;
            }

            return false;
        }
    }
}