﻿// -----------------------------------------------------------------------
// <copyright file="PhotoshopCalls.cs" company="Sector12">
//     Copyright (c) Sector12 Games. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace SpriteBuilder.Logic
{
    using System;
    using System.IO;
    using Photoshop;
    using Sector12Common.PhotoshopHelpers;

    /// <summary>
    /// Class definition for PhotoshopCalls.cs
    /// </summary>
    public static class PhotoshopCalls
    {
        private static Application _ps;

        /// <summary>
        /// This value is necessary because of photoshop's stupidity. Even checking
        /// to see if photoshop is visible (m_ps.Visible) will throw an exception 
        /// if photoshop is busy doing something else (like selecting/editing text).
        /// Thus, we'll keep this variable here, and only access the real value
        /// from within photoshop when we knows it's safe to.
        /// </summary>
        private static bool _isPhotoshopVisible;

        /// <summary>
        /// Initializes photoshop for use.
        /// </summary>
        /// <param name="hidePhotoshopWhileBuilding"></param>
        public static void InitializePhotoshop(bool hidePhotoshopWhileBuilding)
        {
            // Start up photoshop, if it is not already open.
            _ps = new Application();

            // Set photoshop variables
            _ps.Preferences.RulerUnits = PsUnits.psPixels;
            _ps.Preferences.TypeUnits = PsTypeUnits.psTypePixels;
            _ps.DisplayDialogs = PsDialogModes.psDisplayNoDialogs;

            // Hide photoshop if desired.
            _ps.Visible = !hidePhotoshopWhileBuilding;
        }

        /// <summary>
        /// Runs a "BrushStroke" effect on the active document and save it to disk.
        /// </summary>
        /// <param name="absoluteFileName"></param>
        /// <param name="actionName"></param>
        public static void ExecuteAndSavePhotoshopAction(string absoluteFileName, string actionName)
        {
            // Run our brush stroke filter on the active document
            Uri uri = new Uri("pack://application:,,,/Content/PhotoshopActions/SpriteBuilder Actions.atn");
            PhotoshopActionHelpers.ExecuteAction(
                _ps,
                uri,
                "SpriteBuilder Actions",
                actionName);

            // Save the new document as a png file
            _ps.ActiveDocument.Export(absoluteFileName, PsExportType.psSaveForWeb);

            // Revert any changes made by the filter
            RevertToInitializedHistoryState();
        }

        /// <summary>
        /// Loads a .psd file into photoshop.
        /// </summary>
        /// <param name="file"></param>
        public static void LoadPhotoshopFile(FileInfo file)
        {
            // Load the photoshop file
            _ps.Load(file.FullName);

            // Created a names snapshot that we can revert back to in the future
            PhotoshopSnapshotHelpers.CreateNamedSnapshot(_ps, "initialized snapshot");
        }

        /// <summary>
        /// Closes the current photoshop document. The user is not prompted to
        /// save. All changes are discarded.
        /// </summary>
        public static void CloseCurrentDocumentWithoutSaving()
        {
            _ps.ActiveDocument.Close(PsSaveOptions.psDoNotSaveChanges);
        }

        /// <summary>
        /// Should be called after all zones have been built. This will close
        /// photoshop if desired, otherwise, it will ensure photoshop is visible
        /// again if it was hidden during the build.
        /// </summary>
        /// <param name="keepPhotoshopOpen"></param>
        public static void FinalCleanup(bool keepPhotoshopOpen)
        {
            // If photoshop has not been initialized, the user has closed it,
            // or we never opened it (if we're only rebuilding .tmx files for
            // example), then don't do anything.
            if (_ps == null)
                return;

            // Close photoshop if desired. If not, make sure we set it back
            // to visible (if it was set to hidden for the build)
            if (!keepPhotoshopOpen)
                _ps.Quit();
            else
                ShowPhotoshop();
        }

        /// <summary>
        /// Shows the photoshop window. Even if the HidePhotoshopWhileBuilding flag is
        /// set, we still want to make photoshop visible when paused and when the 
        /// build is fully completed.
        /// </summary>
        public static void ShowPhotoshop()
        {
            // Don't just set the value. If photoshop is performing another task
            // (for example, the user is modifying text or styles), chaning the
            // visibility will throw an error stating that photoshop is busy.
            if (!_isPhotoshopVisible)
            {
                _ps.Visible = true;
                _isPhotoshopVisible = true;
            }
        }

        /// <summary>
        /// Hides the photoshop window. This is used when the HidePhotoshopWhileBuilding
        /// flag is set.
        /// </summary>
        public static void HidePhotoshop()
        {
            if (_isPhotoshopVisible)
            {
                _ps.Visible = false;
                _isPhotoshopVisible = false;
            }
        }

        /// <summary>
        /// Reverts our document to a good starting state. This is not actually the
        /// initial starting state the document was in when loaded, but rather a 
        /// state that we're in after the document has been loaded and initialized.
        /// This state will include any logic we want to perform on the document
        /// before we begin building. We don't want to forget this initialization
        /// logic if we revert the image to it's starting state.
        /// </summary>
        private static void RevertToInitializedHistoryState()
        {
            PhotoshopSnapshotHelpers.RevertToNamedSnapshot(_ps, "initialized snapshot");
        }
    }
}